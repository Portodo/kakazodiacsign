<!DOCTYPE html>
<html>
<head>
	<title>Activity PHP</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/slate/bootstrap.css">

</head>
<body>

	<body class="bg-primary">
	<h1 class="text-center text-white my-5 text-danger">ZODIAC FORTUNE</h1>
	<div class="col-lg-4 offset-lg-4 text-danger">
			<form class="bg-info p-4" action="controllers/process.php" method="POST">
				<div class="form-group">
					<label for="fullName">Enter your name</label>
					<input type="text" name="fullName" class="form-control">
				</div>
				
				<div class="form-group">
					<label for="birthMonth">Birth Month</label>
					<input type="text" name="birthMonth" class="form-control">
				</div>

				<div class="form-group">
					<label for="birthDate">Birth Date</label>
					<input type="text" name="birthDate" class="form-control">
				</div>

				<div class="text-center">
					<button type="submit" class="btn btn-danger">See your Future</button>
				</div>

				<?php 
					session_start();
					session_destroy();
					if(isset($_SESSION['errorMsg'])){
				?>
				
					<p class="text-danger"><?php echo $_SESSION['errorMsg'] ?></p>
				<?php		
					}
				?>

		
			</form>

	</div>

</body>
</html>