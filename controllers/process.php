<?php 
	
	$fullName = $_POST['fullName'];
	$birthMonth = strtolower($_POST['birthMonth']);
	$birthDate = $_POST['birthDate'];
	$zodiac = "";
		if($birthMonth === "march" && $birthDate >=21){
			$zodiac = "Aries";
		}elseif($birthMonth === "april" && $birthDate <=19){
			$zodiac = "Aries";
		}elseif($birthMonth === "april" && $birthDate >=20){
			$zodiac = "Taurus";
		}elseif($birthMonth === "may" && $birthDate <=20){
			$zodiac = "Taurus";
		}elseif($birthMonth === "may" && $birthDate >=21){
			$zodiac = "Gemini";
		}elseif($birthMonth === "june" && $birthDate <=20){
			$zodiac = "Gemini";
		}elseif($birthMonth === "june" && $birthDate >=21){
			$zodiac = "Cancer";
		}elseif($birthMonth === "july" && $birthDate <=22){
			$zodiac = "Cancer";
		}elseif($birthMonth === "july" && $birthDate >=23){
			$zodiac = "Leo";
		}elseif($birthMonth === "august" && $birthDate <=22){
			$zodiac = "Leo";
		}elseif($birthMonth === "august" && $birthDate >=23){
			$zodiac = "Virgo";	
		}elseif($birthMonth === "september" && $birthDate <=22){
			$zodiac = "Virgo";
		}elseif($birthMonth === "september" && $birthDate >=23){
			$zodiac = "Libra";
		}elseif($birthMonth === "october" && $birthDate <=22){
			$zodiac = "Libra";
		}elseif($birthMonth === "october" && $birthDate >=23){
			$zodiac = "Scorpio";
		}elseif($birthMonth === "november" && $birthDate <=21){
			$zodiac = "Scorpio";
		}elseif($birthMonth === "november" && $birthDate >=22){
			$zodiac = "Sagitarrius";
		}elseif($birthMonth === "december" && $birthDate <=21){
			$zodiac = "Sagitarrius";
		}elseif($birthMonth === "december" && $birthDate >=22){
			$zodiac = "Capricorn";
		}elseif($birthMonth === "january" && $birthDate <=19){
			$zodiac = "Capricorn";
		}elseif($birthMonth === "january" && $birthDate >=20){
			$zodiac = "Aquarius";
		}elseif($birthMonth === "february" && $birthDate <=18){
			$zodiac = "Aquarius";
		}elseif($birthMonth === "february" && $birthDate >=19){
			$zodiac = "Pisces";
		}elseif($birthMonth === "march" && $birthDate <=20){
			$zodiac = "Pisces";
		}else{
			$zodiac = "";
		}


	session_start();
	if (strlen($fullName) === 0 || strlen($birthMonth) ===0 || strlen($birthDate) ===0 || $birthDate > 31 || $zodiac ==""){
			header("Location: ". $_SERVER['HTTP_REFERER']);	
			$_SESSION['errorMsg'] = "Please fill up the form properly";
	}else{
		
		$_SESSION['fullName'] = $fullName;
		$_SESSION['birthMonth'] = $birthMonth;
		$_SESSION['birthDate'] = $birthDate;
		$_SESSION['zodiac'] = $zodiac;

		header("Location: ../views/landingpage.php");
		
	}


?>